/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import AppContainer from "./navigation/AppNavigator"
import {name as appName} from './app.json';
//Database import
import * as firebase from 'firebase'
var config = {
  apiKey: "AIzaSyCLWkxnTVNtgQjy-yxepdDrOW8kKsWTU2M",
  authDomain: "quiz-chatbot.firebaseapp.com",
  databaseURL: "https://quiz-chatbot.firebaseio.com",
  projectId: "quiz-chatbot",
  storageBucket: "quiz-chatbot.appspot.com",
  messagingSenderId: "316769053953",
  appId: "1:316769053953:web:8eb59a4adb1191c1"
};
firebase.initializeApp(config);
AppRegistry.registerComponent(appName, () => AppContainer);
