import React, {Component} from 'react';
import {
  Image,
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView ,
} from 'react-native';
import InAppBilling from "react-native-billing";
import AsyncStorage from '@react-native-community/async-storage';

const defaultState = {
    productDetails: null,
    transactionDetails: null,
    consumed: false,
    unlocked : false,
    error: null
  };

export default class MainScreen extends Component {
    constructor(props){
        super(props)
        this.state = {
            productId: "android.test.purchased", //id of your product
            ...defaultState
        };
    }

    componentDidMount = async () => {
        this.willFocus = this.props.navigation.addListener ('willFocus', () =>{
            this.checkPurchase() // check if user has purchase

        })
    }

    componentWillUnmount() {
        // Remove the event listener before removing the screen from the stack
        this.willFocus.remove();
    }

    resetState = () => {
        this.setState(defaultState);
    };

    
    async checkPurchase() {
        console.log("check purchase")
         try {
         this.resetState();
         await InAppBilling.open();
         // If subscriptions/products are updated server-side you
         // will have to update cache with loadOwnedPurchasesFromGoogle()
         await InAppBilling.loadOwnedPurchasesFromGoogle();
         let isPurchased = await InAppBilling.isPurchased(this.state.productId)
         console.log("Customer purchased: ", isPurchased);
         await InAppBilling.close();
         const storage = await AsyncStorage.getItem("isPurchased")
         isPurchased = storage ? JSON.parse(storage).isPurchased : false
         this.setState({isPurchased})
       } catch (err) {
         console.log(err);
         await InAppBilling.close();
       } 
     }

    purchaseProduct = async () => {
        try {
            if(this.state.isPurchased)
            {
                this.props.navigation.navigate("Auth") 
            }else{
                this.resetState();
                await InAppBilling.open();
                const details = await InAppBilling.purchase(this.state.productId);
                await InAppBilling.close();
                this.setState({ transactionDetails: JSON.stringify(details) });
                console.log(JSON.stringify(details),"bill")
                switch(details.purchaseState)
                {
                  case "PurchasedSuccessfully":
                      await AsyncStorage.setItem('isPurchased', JSON.stringify({isPurchased:true}))
                      alert("Congratulations you already unlock new features")
                      this.props.navigation.navigate("Auth") 
                      break
                  case "SubscriptionExpired":
                      alert("Your subscription has expired")
                      break
                }
            }

        } catch (err) {
            this.setState({ error: JSON.stringify(err) });
            console.log(JSON.stringify(err) ,"error")
            await InAppBilling.close();
        }
    };

    unlockFeatures = () => {
        const { isPurchased } = this.state
        console.log("isPurchased",isPurchased)
        this.setState({unlocked : isPurchased})
    }

        //delete purchase cache
        consumePurchase = async () => {
            try {
              this.resetState();
              await InAppBilling.open();
              const details = await InAppBilling.consumePurchase(this.state.productId);
              await InAppBilling.close();
              this.setState({ consumed: true, unlocked: false });
              console.log(JSON.stringify(details) ,"details")
            } catch (err) {
              this.setState({ error: JSON.stringify(err) });
              console.log(JSON.stringify(err) ,"error")
              await InAppBilling.close();
            }
        };
    

    render(){
        return (
            <ScrollView style={{marginvertical: 0}}>
                <View style={{ flexDirection: 'row', marginTop:30 }}>
                    <TouchableOpacity onPress={ () => this.props.navigation.toggleDrawer()}>
                        <Image
                            source={require('../assets/images/drawer.png')}
                            style={{ width: 25, height: 25, marginLeft: 5 }}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.bigcontainer}>
                    <Text style={styles.title}></Text>
                    <Image 
                        style={styles.logo}
                        source={require('../assets/images/robot-dev.png')}  
                    />
                    <Text style={styles.feature}>Apps</Text>
                </View>
                <View style={styles.bigcontainer}>
                    <Text style={styles.feature2}></Text>
                    <Text style={styles.feature}>{''} </Text>
                    <Text style={styles.feature}>{''} </Text>
                    <Text style={styles.feature}>{''} </Text>
                    <Text style={styles.feature}>{''} </Text>
                </View>
                <ScrollView contentContainerStyle={styles.container}
                    directionalLockEnabled={true}
                    vertical={true}>
                    <TouchableOpacity onPress={()=> this.purchaseProduct()} style={styles.scrollcontainer}>
                        <Text style={styles.buttonText2}>All Time Unlock new features</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.scrollcontainer}
                        onPress={() => this.props.navigation.navigate("test1") }>
                        <Text style={styles.buttonText2}>Test1 </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.scrollcontainer}
                        onPress={() => this.props.navigation.navigate("test2") }>
                        <Text style={styles.buttonText2}>Test2</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.scrollcontainer}
                        onPress={() => this.props.navigation.navigate("test3") }>
                        <Text style={styles.buttonText2}>Test3</Text>
                    </TouchableOpacity>
                </ScrollView>
            </ScrollView>
        )
    }
}



const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
    },
    imgBackground:{
        flex:1
    },
    logo: {
        width: 100,
        height: 100,
        resizeMode: 'contain',
        justifyContent: 'center'
    },
    welcome:{
        fontSize:14,
        color:'#000',
        fontWeight:'400',
        textAlign: 'center'
   },
   welcome2:{
        fontSize:14,
        color:'#000',
        fontWeight:'400',
        textAlign: 'center'
    },
    buttonContainer:{
        height:30,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical:10,
        width:200,
        borderRadius:10,
	    fontWeight: '500',
        backgroundColor: "#1F93FF",
    },
    bigcontainer:{
        height:220,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical:10,
        marginHorizontal:30,
        width:300,
        textAlign:'center',
        borderTopLeftRadius: 34,
        borderTopRightRadius: 34,
        borderBottomRightRadius: 34,
        fontWeight: '500',
        backgroundColor: "#ffffff",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.36,
        shadowRadius: 6.68,
        elevation: 11,   
    },
    smallcontainer:{
        height:90,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical:5,
        marginHorizontal:30,
        width:300,
        textAlign:'center',
        borderRadius: 10,
	    fontWeight: '500',
        backgroundColor: "#1F93FF",
        shadowColor: "#405eff",
        shadowOffset: {
	        width: 0,
	        height: 3,
        }    ,
        shadowOpacity: 0.36,
        shadowRadius: 6.68,
        elevation: 11,   
    },
    scrollcontainer:{
        height:40,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical:5,
        marginHorizontal:30,
        width:300,
        textAlign:'center',
        borderRadius: 3,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
	    fontWeight: '500',
        backgroundColor: "#1F93FF",
    },
    buttonText:{
        padding: 4,
        fontSize: 16,
        textAlign: 'center',
        fontWeight: '500',
        color : '#ffffff'
    },
    buttonText2:{  
        padding: 4,
        fontSize: 18,
        textAlign: 'center',
        fontWeight: '600',
        color : '#fff'
    },
    buttonText3:{  
        padding: 4,
        fontSize: 40,
        textAlign: 'center',
        fontWeight: '700',
        color : '#1F93FF'
    },
    title:{
        fontSize: 24,
        fontWeight: '600',
        paddingVertical:10,
        fontWeight: 'bold'
    },
    feature2:{
        padding: 4,
        fontSize: 16,
        textAlign: 'center',
        fontWeight: '500',
        color : '#000'
    },
    feature:{
        padding: 4,
        fontSize: 16,
        textAlign: 'center',
        fontWeight: '500',
        color : '#1F93FF'
    }
})